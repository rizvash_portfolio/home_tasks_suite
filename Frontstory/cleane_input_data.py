import os
import csv
import sys

import pandas as pd

from datetime import datetime


try:

    source_path_file_revenue = r"./revenue_1.csv"
    source_path_file_cost = r"./cost_1.csv"

    # log file to write to an Error
    errors_log_file = r'./errors_log.txt'


    #    descript_info_patterns_revenue = ["data_date", "campaign_id", "revenue"]
    #    descript_info_patterns_cost = ["data_date", "campaign_id", "campaign_name", "clicks", "cost"]
    #
    #    descript_info_patterns_output = ["roi", "profit", "avg_uv", "avg_cpc", "positive_profit_hours",
    #                                     "total_revenue", "total_cost", "total_clicks", "day", "campaign_name",
    #                                     "campaign_id"]



    with open(source_path_file_revenue, 'r') as source_f1, \
            open(source_path_file_cost, 'r') as source_f2, \
            open(errors_log_file, 'a') as errors_log:

        #### Processing file_revenue
        source_f1 = csv.reader(source_f1)
        next(source_f1) # skip headers
        source_f1_processing_result = []

        for parsed_line in source_f1:  # revenue_1 file
            print(parsed_line)
            # Validation file revenue
            if len(parsed_line) != 3: # Validation length of parsed line
                errors_log.write(f" Invalid length row: {parsed_line} ! \n")
                continue
            try: # Validation `revenue` value
                float(parsed_line[2])
            except Exception as e:
                errors_log.write(f"{parsed_line} \n")
                continue

            # Convert EST in UTC timezone and splited to Date Time columns
            datetime_str = parsed_line[0]
            datetime_object = datetime.strptime(datetime_str, '%m/%d/%y  %H:%M')
            date = datetime_object.strftime("%Y-%m-%d")
            time = datetime_object.strftime("%H:%M:%S")

            f1_processing_result = {
                "Date": date,
                "Time" : time,
                "Campaign_id" : parsed_line[1],
                "Revenue": parsed_line[2]
            }

            source_f1_processing_result.append(f1_processing_result)

        df_revenue = pd.DataFrame(source_f1_processing_result)

        df_revenue.to_csv("clean_revenue.csv",encoding='utf-8')

        #### Processing file_cost_1
        source_f2 = csv.reader(source_f2)
        next(source_f2) # skip headers


        source_f2_processing_result = []

        for parsed_line in source_f2:  # cost_1.csv file

            if len(parsed_line) != 5: # Validation length of parsed line
                errors_log.write(f" Invalid length row: {parsed_line} ! \n")
                continue
            try: # Validation `revenue` value
                int(parsed_line[3])
                float(parsed_line[4])
            except Exception as e:
                errors_log.write(f" Invalid `revenue` value in row: {parsed_line} ! \n")
                continue

            # Convert EST in UTC timezone and splited to Date Time columns
            datetime_str = parsed_line[0]
            datetime_object = datetime.strptime(datetime_str, '%m/%d/%y %H:%M')
            date = datetime_object.strftime("%Y-%m-%d")
            time = datetime_object.strftime("%H:%M:%S")

            # descript_info_patterns_cost = ["data_date", "campaign_id", "campaign_name", "clicks", "cost"]
            f2_processing_result = {
                "Date": date,
                "Time" : time,
                "Campaign_id" : parsed_line[1],
                "Campaign_name" : parsed_line[2],
                "Clicks": parsed_line[3],
                "Cost": parsed_line[4]
            }

            source_f2_processing_result.append(f2_processing_result)
        df_cost = pd.DataFrame(source_f2_processing_result)
        df_cost.to_csv("clean_cost.csv", encoding='utf-8')


except EOFError as ex:
    raise  ex

except (IOError, ValueError, EOFError) as ex:
    raise  ex

except:
    print(f"Unexpected error: {sys.exc_info()[0]}")
    raise
