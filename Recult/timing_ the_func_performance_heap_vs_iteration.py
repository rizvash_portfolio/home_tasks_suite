
""" 
According to the conditions of the problem:
- the day of purchase a stock it is a random value from given range (list)
Task: Find max possible profite
Find sell point for receiving the max possible  profit from sell the stock in remain days
(considering only days after day of buying the stock)


Given solution "optimal two transactions" do not cover functionality "pick up random day for buy stock".
It starts from the first element of given array so output results of function seems to will be wrong.
 
So I edited it for correct compare of time complexit

 def optimal_two_transactions(stocks):
	if len(stocks) < 2:
		return
	minnum = stocks[0]
	maxsum = 0
	for num in stocks[1:]:
		if num < minnum:
			minnum = num
		elif num-minnum > maxsum:
			maxsum = num-minnum
	return maxsum 
 """

"""
   For test the performance of a functions we will use only correct input- index not out of range
   index_buy_day = randrange(0, (len(stocks)-2)) # index of  `buy day`
"""

from random import randrange
import random
import heapq
import time

# Solution 1 Iterations
def optimal_two_transactions(start_day, stocks):
	"""
	Iterations way.
	"""
	if len(stocks) < 2:
		return
	minnum = stocks[start_day]
	maxsum = 0
	for num in stocks[start_day+1:]:
		if num < minnum:
			minnum = num
		elif num-minnum > maxsum:
			maxsum = num-minnum
	return maxsum


# Solution 2
def heap_approach(start_day, stocks):
	""" Heap way. """
	try:
		buy_price = stocks[start_day]  # the buy price
		stocks = stocks[start_day + 1:]  # slice from next after `buy day` to the end of array
	except IndexError:
		print("The day index is out of range. Please set another day")

	heapq._heapify_max(stocks)
	best_price = stocks[0]
	return best_price - buy_price


def time_comparison(func_1, func_2, n_iter=100, items_in_list = 1000):
	"""
	Functions run 100 times (n_iter=100) for avoid any side effects.
	So as a result we will have the minimum value from all func runs.
	"""
	acc_func_1 = float("inf")
	acc_func_2 = float("inf")
	stocks = [random.randrange(0, 500) for items in range(items_in_list)] # set number of elements curent val=2000
	start_day = randrange(0, (len(stocks) - 2)) # For time test I will use only correct input- index not out of range
	for i in range(n_iter):
		t_start_func_1 = time.perf_counter()
		func_1(start_day, stocks)
		t_end_func_1 = time.perf_counter()
		acc_func_1 = min(acc_func_1, t_end_func_1 - t_start_func_1)

		t_start_func_2 = time.perf_counter()
		func_2(start_day, stocks)
		t_end_func2 = time.perf_counter()
		acc_func_2 = min(acc_func_2, t_end_func2 - t_start_func_2 )

	return acc_func_1, acc_func_2

if __name__ == "__main__":

	# Test section
	assert optimal_two_transactions(start_day=0, stocks=[1, 3, 2, 4]) == 3, "Houston we've got a problem with Itereations"
	# optimal_two_transactions  - not handle negative profit results. Case when buy price is greater than sell price

	assert heap_approach(start_day=0, stocks=[1, 3, 2, 4]) == 3, "Houston we've got a problem with Heap approach"
	# negative profit results:
	assert heap_approach(start_day=1,
						 stocks=[1, 5, 4, 2, 2, 2]) == -1, "Houston we've got a problem with Itereations"


	# Comparison the time differences results.
	# Set an `items_in_list` value (number of items in the input list) and run it.
	# - !!! Min items_in_list value = 3 !!! -

	results = time_comparison(heap_approach, optimal_two_transactions, items_in_list=2000)

	print(results)
	print(f"Min time result of Heap approach: {results[0]}, Min time result for Iteretion approach {results[1]}.")
	if results[0] < results[1]:
		print(f"Heap approach faster then Iteration way approximately in {round(results[1] / results[0], 4)} times.")
	elif round(results[0], 8) == round(results[1], 8):
		print(f"The both approaches results are equal to each other")
	else:
		print(f"Iteration approach faster then Heap way approximately in {round(results[0] / results[1], 4)} times.")

