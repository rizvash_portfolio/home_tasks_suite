import csv
from faker import Faker
import useful_functions_param_weight_range as myfunc

faker = Faker()


class Candidate:   # MAX TOTAL SCORE 105 points
    def __init__(self):
        self.Person_Name = myfunc.get_random_name()  # Simple random name
        self.Years_of_Expirience = myfunc.get_random_expiriance()  #  max 10 point /  min 3
        self.Coding_skill = myfunc.get_random_positive_r10()  # clean, readable, effective, testable  # max 10 point / min 1
        self.System_architect_knowledge = myfunc.get_random_positive_r5()  # OnPrime, Cloud # # max 5 point / min 0
        self.Common_knowledge = myfunc.get_random_positive_r10()  # Git, Docker, DB (Relational NoSQL), Metodological  # max 10 point / min 1
        self.Algorithms_data_struct = myfunc.get_random_positive_to_10()  # liniar, recursion, graddy, heap, trees, # max 10 point / min 0
        self.Soft_Skill = myfunc.get_random_positive_r5()  # Management, Leadership, responsibilities  # max 5 point / min 0
        self.Recommendation = myfunc.get_random_recommendation()  # Negative: -1, Neutral: 0, Positive: 1, SuperPositive: 2 # max 2 point min -1
        self.Education = myfunc.get_random_education()  # Self Learninig( hands on practic): 2p, 1st Degree 2p, 2nd- degree 3p, 3rd - PhD 5p # max 5 point / min 2
        self.First_impression = myfunc.get_random_personal_impression()  # positive / negative  max 5 point / min -3
        self.Communication = myfunc.get_random_score_n5_p10()  # max 10 point / min -5
        self.Willness_to_learn = myfunc.get_random_score_n5_p10() # max 10 point / min -5
        self.Certificates = myfunc.get_random_certificate() # max 3 point / min 0
        self.Willness_to_knowledge_sharing = myfunc.get_random_score_n5_p10() # max 10 point / min -5
        self.Motivation = myfunc.get_random_score_n5_p10() # max 10 point / min -5

    @property
    def get_total_score(self):
        total_score: int = 0
        for k, v in self.__dict__.items():
            if (isinstance(v, int)):
                total_score += v
            else:
                pass
        return total_score


if __name__ == "__main__":
    candidate_list = []  # for pandas/df approach
    with open('b_all_candidate_1million_list.csv', 'a', newline='') as csv_file:

        c = Candidate()
        c.__dict__.update({"Totall_Score": c.get_total_score})
        writer = csv.DictWriter(csv_file, fieldnames=list(c.__dict__.keys()))
        writer.writeheader()

        for i in range(1000000):  # Set number of population size
            c = Candidate()
            c.__dict__.update({"Totall_Score": c.get_total_score})
            writer.writerow(c.__dict__)  # Comment the row if you want create `dict_view_file.csv`

"""
'''
#Un comment block and comment the above row if you want create `dict_view_file.csv`
# like: "('Person_Name', 'Marcus Flores')","('Years_of_Expirience', 10)","('Coding_skill', 8)",.....)"
            with open('dict_view_file.csv', 'a', newline='') as csv_file:
                writer = csv.writer(csv_file)
                writer.writerow(c.__dict__.items())
'''
"""
