The decision to hire or not in most cases an intuitive decision.
But let looking on the solution of this problem with from technical (engineering) approach.

Please read `HowTo.md`
Simple output example you can find in `Results_in_HTML_view` folder.

The goals of investigation is provide technical answer for the questions:
 a) Deviation from mean/mode of "the best candidates sample" for given candidate.
 b) How the skills of the given candidate will affect to the "Team Skills"   


Please use Jupyter Notebook ( or PyCharm Scientific mode ) for viewing results and editing parameters.

- 1 Step create general population of 1 000 000 candidates (populate_random_candidates_class_pg.py)
   General Population max/ min value description: 
   The possible max/ min Total Score:   -> ( But it seems to be an Outliers)
   MAX TOTAL SCORE 105 points 
   MIN TOTAL SCORE -17 points 
   TOTAL RANGE 122 points

- 2 Create samples (csv files) best 100k, 50k and 10k candidates base on "Totall Score "
- 3 Сalculate base Descriptive statistics for each of sample.

#### Play with Data Samples / Clustering:
 - Find optimal optimal number of clusters -> Clustering_KMeans_elbow_optimal_k.ipynb
 - Clustering data with k means approach->clustering_samples.ipynb
 
 Play around ideas:
 - Change Mean values to Median
 - Change Mean values to the bigest Mode