0. Setup env and install requirements: `run: pip3 install -r requirements.txt in your shell`
1. Set the candidate scores in `set_the_candidate_score_dict.py`
2. Set the desired weight range for each parameter in `useful_functions_param_weight_range.py`
3. Generate general population of candidates (~ 1million person) start:  `populate_random_candidates.py`
4. Create samples (+ csv files) of the_best candidates 10k, 50k, 100k : `Create_samples_csv_the_best_10k50k100k.ipynb`
5. Descriptions statistics and plot of 'best candidates' samples : `Best_candidates_samples_description.ipynb`
6. Find optimal N clusters with plot "elbow optimal k" : `Clustering_KMeans_elbow_optimal_k.ipynb`
7. Clustering data. According plot result the optimal k result ~ 9
8. The given candidate scoring analysis results: `Given_candidate_scoring_analysis.ipynb`
9. Set the Team Scoring in file `TeamScoring.xlsx`
9.1 Result of 'How the Skills of the given candidate impact to the team results':
 in `Given_candidate_inpact_Team_scoreboard_analysis.ipynb`
 
10. For simple review results (no IDE or code run) please use html files in `Results_in_HTML_view` folder.   