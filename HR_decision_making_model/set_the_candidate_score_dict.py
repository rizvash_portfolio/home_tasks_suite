
the_candidate = {
# 'Person_Name' : "My Name",
# 'Totall_Score' : 0,
 'Years_of_Expirience': 7,             #  (Min = 3,  Max = 10) point. Years of Expirience.
 'Coding_skill': 7.1,                  #  (Min = 1,  Max = 10) point. Clean, readable, effective, testable.
 'System_architect_knowledge': 6,      #  (Min = 0,  Max = 5) point.  OnPrime, Cloud etc.
 'Common_knowledge': 7,                #  (Min = 1, Max = 10) point.  Git, Docker, DB(Relational NoSQL), Metodological
 'Algorithms_data_struct': 6,          #  (Min = 0, Max = 10) point.  Liniar, recursion, graddy, heap, trees etc.
 'Soft_Skill': 7,                      #  (Min = 0, Max = 5 ) point.  Management, Leadership, responsibilities...
 'Recommendation': 1,                  #  (Min = -1, Max = 2) point.  Negative: -1, Neutral: 0, Positive: 1, SuperPositive: 2
 'Education': 3,                       #  (Min = 2, Max = 5) point.   Self Learn(hands on practic): 2p, 1stDeg. 2p, 2ndDegr. 3p, 3rd PhD
 'First_impression': 4,                #  (Min = -3, Max = 5) point.  / negative / neutral/ Positive..
 'Communication': 6,                   #  (Min = -5, Max = 10) point. Communication skills and languages.
 'Willness_to_learn': 6.5,             #  (Min = -5, Max = 10) point. Willingness to learn new technologies.
 'Certificates': 3,                    #  (Min = 0, Max = 3 ) point.  Certificates and eLearning.
 'Willness_to_knowledge_sharing': 6,   #  (Min = -5, Max = 10) point. Willness to collaboration and knowledge sharing.
 'Motivation': 7                       #  (Min = -5 , Max =10 ) point. Motivation as is.
 }