import random
from faker import Faker


def get_random_positive_r10():
    return (random.randint(1, 10))    # max 10 point

def get_random_positive_r5():
    return (random.randint(0, 5))  # max 5 point

def get_random_positive_to_10():
    return (random.randint(0, 10))  # max 10 point

def get_random_score_n5_p10():
    return (random.randrange(-5, 10 + 1)) # max 10 point / min -5p

def get_random_personal_impression():
    possible_first_impressions = [-3, 0, 4, 5 ]    # ["Negative": -3, "Netral": 0, "Positive": 4, "SuperPositive": 5]
    return random.choice(possible_first_impressions)  #max 5 point

def get_random_expiriance():
    number_of_years_in_industry = [3, 3, 4, 4, 4, 5, 5, 5, 5, 5, 6, 6, 7, 7, 8, 9, 10] # max 10 point
    return random.choice(number_of_years_in_industry)

def get_random_soft_skils():
    return (random.randint(1, 5)) # max 5 point

def get_random_expiriance_level():
    return (random.choice([2, 2, 2, 3, 3, 5])) # max 5 point

def get_random_certificate():
    return (random.choice([0, 1, 1, 2, 2, 2, 3, 3])) # max 3 point

def get_random_education(): #Self Learninig( hands on practic): 2p, 1st Degree 2p, 2nd- degree 3p, 3rd - PhD 5p
    return (random.choice([2, 2, 2, 3, 3, 5])) # max 5 point

def get_random_recommendation():  #max 2 point
    return (random.choice([-1, 0, 1, 2])) # Negative -1, Netral - 0, Positiv  - 1, SuperPositive - 2

faker = Faker()
def get_random_name():
    name = faker.name()
    return name


#print(get_random_expiriance_level())