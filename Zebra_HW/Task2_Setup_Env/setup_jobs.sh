#!/usr/bin/sh

# !!!! --- Make it executable: chmod +x setup_jobs.sh  ---!!!

# Usage: This script is for setup Env.
#Steps/jobs:
# 1 - instal re pip freeze > requirements. txt. / pip install -r requirements.txt
# 2 - start docker-compouse file for starting up Postgress and Redis conteiners
# 3 - Create postgresql database (test db)
# 4 - Create postgresql test table in database test_tb ((postgresql)


echo $(docker-compose up -d) && sleep 2 && \
echo 'Docker-Compose Started OK' &&/ sleep 3 && \
echo $(pip install -r ./requirements.txt) && \
echo 'Requirements installed - OK' && sleep 7


python ./postgres_create_test_db.py && \
echo 'Postgress test DB created!' && sleep 3 &&
python ./postgres_creat_test_table.py && \
echo 'Postgress Test Table created!' && sleep 3 && \
echo 'Environment setup pass OK! You can proceed with the tasks!'
sleep 7

#exit 0