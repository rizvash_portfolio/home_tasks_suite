# !/usr/bin/python3

import psycopg2


from config import *

connection = None
try:
    connection = psycopg2.connect(database=DB_NAME, user="postgres",
                                  password="postgres", host="localhost", port="5432")
    connection.autocommit = True
    cur = connection.cursor()
except (Exception, psycopg2.Error) as error:
    print("Error while connecting to PostgreSQL", error)
    cur.close()
    connection.close()

try:
    cur.execute(f"CREATE TABLE IF NOT EXISTS {TEST_TABLE} "
                f"(id serial PRIMARY KEY, "
                f"hashed_study_id varchar(256),"
                f" series_id varchar(128),"
                f" dcm_id varchar(128));")
except (Exception, psycopg2.Error) as e:
    print(f"Error creating table: {e}!")
finally:
    cur.close()
    connection.commit()
    connection.close()

# I understand it's look like "OverKill" but please - do not remove it.
cur.close()
connection.close()
