BASE_PATH_WINDOWS = r"E:/"
BASE_PATH_LINUX = "/usr/local/"  # /home
BASE_PATH_OSX = "/usr/local/"
PROJECT_NAME = "Zebra_HW_folder"
NUMBER_OF_STUDY_DIR = 100
FILE_EXTENSION = '.dcm'

# REDIS
REDIS_LIST = 'dcm_file_paths'
REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_PASSWORD = ''

# PostgreSQL
# docker run -p 5432:5432 --name postgres -d postgres:11.3
DB_NAME = 'test_db'
DB_USER = 'db_user'
DB_PASSWORD = 'mypassword'
DB_HOST = 'localhost'
DB_PORT = 5432
TEST_TABLE = "Test_table"

#  valid_extention_dict = ['.DCM', '.DICOM'] # DCM files may also be saved as .DICOM files.
