# !/usr/bin/python3

import psycopg2
import os
import config
from config import *
#from hw_zebra.Task2_Setup_Env.config import *
#from hw_zebra.config import DB_NAME

connection = None
try:
    connection = psycopg2.connect("user='postgres' host='localhost' password='postgres' port='5432'")
    connection.autocommit = True
except (Exception, psycopg2.Error) as e:
    print(f"Error while connecting to PostgreSQL: {e}")
    if connection:
        connection.close()
# Validation the data base name:
if connection is not None:
    cur = connection.cursor()
    cur.execute("SELECT datname FROM pg_database;")
    list_database = cur.fetchall()
    new_database_name = DB_NAME

    if (new_database_name,) in list_database:
        print(f"{new_database_name} Database already exist")
    else:
        try:
            sqlCreateDatabase = "create database " + DB_NAME;
            cur.execute(sqlCreateDatabase)
            connection.commit()
            connection.close()
            cur.close()
        except (Exception, psycopg2.Error) as e:
            print(f"Error create new DB : {e}")
            if connection:
                cur.close()
                connection.close()
        finally:
            cur.close()
            connection.close()

# I understand it's look like "OverKill" but please - do not remove it.
cur.close()
connection.close()