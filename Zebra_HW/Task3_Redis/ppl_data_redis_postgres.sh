#!/usr/bin/sh

# !!!! --- Make it executable: chmod +x setup_jobs.sh  ---!!!

# Usage: This script is for run data pipeline:
# extract data from file system -> Put in Redis DB -> extract data from Redis ->
# -> data wrangling (hash folder name) -> write data to table in PostgreSQL DB
echo "START Pipline"
sleep 4
python redis_data_producer.py && echo 'Set data to Redis - Pass OK!'
sleep 6
python redis_consumer_to_db.py && echo 'hash data and set it to PostgreSQL - Pass OK!'
sleep 6