# !/usr/bin/python3

import hashlib
import psycopg2
import redis
from hw_zebra.config import PROJECT_NAME, REDIS_HOST, REDIS_PORT, REDIS_LIST, DB_NAME, TEST_TABLE

print("Redis consumer started!")
connection = None
try:
    connection = psycopg2.connect(database=DB_NAME, user="postgres",
                                  password="postgres", host="localhost", port="5432")
    connection.autocommit = True
    cur = connection.cursor()
except (Exception, psycopg2.Error) as error:
    print("Error while connecting to PostgreSQL", error)
    cur.close()
    connection.close()


def consumer_hash_to_db():
    """
    Retrieving data from the server Redis collection, hash the "study_id" using sha512 and save it to SQL db
    """
    try:  # Get data from Redis
        with redis.StrictRedis(REDIS_HOST, REDIS_PORT) as r:
            assert (r.ping()), "No response from Redis DB"
            items_in_q_flag = True
            while items_in_q_flag:
                val = r.lpop(REDIS_LIST)
                items_in_q_flag = val
                if val == None: break
                # Hash a 'study_id' and set data to PostgreSQL table
                given_path_list = val.decode('ascii').split("/")
                index_pr_name = given_path_list.index(f"{PROJECT_NAME}")
                hashed_study_id = hashlib.sha512((given_path_list[index_pr_name + 1].encode())).hexdigest()  # ->to DB
                assert type(hashed_study_id) == type('str'), "hashed_study_id not a sting"  # Check hashed_study_id
                series_id = given_path_list[index_pr_name + 2]  # ->to DB
                dcm_id = given_path_list[index_pr_name + 3]  # ->to DB

                insert_query = (
                    f"INSERT INTO {TEST_TABLE} "
                    f"(hashed_study_id, series_id, dcm_id) "
                    f"VALUES "
                    f"('{hashed_study_id}', '{series_id}', '{dcm_id}');")

                cur.execute(insert_query)
                connection.commit()

    except (Exception, psycopg2.Error) as e:
        print(f"Error: {e}")

    connection.close()

if __name__ == "__main__":
    consumer_hash_to_db()