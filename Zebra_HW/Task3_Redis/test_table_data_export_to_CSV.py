import csv
import datetime
import os

from hw_zebra.Task2_Setup_Env.config import *
from hw_zebra.useful_functions import *

# SetUP-> File name / Sql Query.
fileName = 'ExportData_' + str(datetime.datetime.now().strftime('%Y%m%d_%H%M%S')) + '.csv'
sqlSelect = f"SELECT hashed_study_id, series_id, dcm_id FROM {TEST_TABLE} ;"

# Create Data Export folder:
base_path = setup_base_path()
try:
    data_export_folder = os.path.join(base_path, PROJECT_NAME, "DataExport")
    if os.path.exists(data_export_folder):
        pass
    else:
        os.mkdir(data_export_folder)
except OSError as Error:
    print(Error)
    raise OSError("Got an error", Error)

# Database connection:
connection = get_connect_to_postgres_db(database=DB_NAME, user="postgres", password="postgres", host="localhost",
                                        port="5432")
cur = connection.cursor()

# Check if the file path exists.
if os.path.exists(data_export_folder):
    try:
        cur.execute(sqlSelect)
        # Fetch the data returned.
        results = cur.fetchall()
        # Extract the table headers.
        headers = [i[0] for i in cur.description]
        # Open CSV file for writing.
        data_path = os.path.join(data_export_folder, fileName).replace('\\', '/')
        csvFile = csv.writer(open(data_path, 'w', newline=''),
                             delimiter=',', lineterminator='\r\n',
                             quoting=csv.QUOTE_ALL, escapechar='\\')
        csvFile.writerow(headers)
        csvFile.writerows(results)
        # Message stating export successful.
        print("Data export successful.")

    except psycopg2.DatabaseError as e:
        print(e)
