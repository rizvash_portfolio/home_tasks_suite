# !/usr/bin/python3
import os
import redis

from hw_zebra.config import REDIS_HOST, REDIS_PORT, REDIS_LIST, PROJECT_NAME
from hw_zebra.useful_functions import setup_base_path



print("Redis producer started!")

base_path = setup_base_path()
source_dir: str = base_path + PROJECT_NAME

def producer():
    """
    Redis producer: Insert  to Redis collection all paths to files
    that end with ".dcm" from all subdirectories to redis list
    """
    try:
        with redis.StrictRedis(REDIS_HOST, REDIS_PORT) as r:
            # with redis.StrictRedis(host='localhost', port=6379) as r:
            assert (r.ping()), "No response from Redis DB"
            print(r.ping())
            for root, dirs, files in os.walk(source_dir):
                for file in files:
                    if file.endswith(".dcm"):
                        name_path = os.path.join(root, file).replace('\\', '/')
                        r.rpush(REDIS_LIST, f'{name_path}')
                        print(name_path)

    except Exception as e:
        print(f'Error: {e}')

if __name__ == "__main__":
     producer()
