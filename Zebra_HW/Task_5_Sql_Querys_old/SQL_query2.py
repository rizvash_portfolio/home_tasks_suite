import psycopg2

from hw_zebra.Task2_Setup_Env.config import *
from hw_zebra.useful_functions import get_connect_to_postgres_db


connection = get_connect_to_postgres_db(database=DB_NAME, user="postgres", password="postgres",
                                        host="localhost", port = "5432")
cur = connection.cursor()

# QUERY_2: Get all studies that have more than 4 series.

select_query2 = (f"SELECT hashed_study_id"
                 f" FROM {TEST_TABLE}"
                 f" GROUP BY hashed_study_id "
                 f"HAVING (COUNT(DISTINCT series_id)) > 4 ;")

cur.execute(select_query2)
records = cur.fetchall()
count_uniq = 0
for row in records:
    print(row)
    count_uniq += 1

print(f"Total study_ids that have more than 4 series: {count_uniq} ")

cur.close()
connection.close()
