import psycopg2

from hw_zebra.Task2_Setup_Env.config import *
from hw_zebra.useful_functions import get_connect_to_postgres_db


connection = get_connect_to_postgres_db(database=DB_NAME, user="postgres", password="postgres", host="localhost", port = "5432")
cur = connection.cursor()


# QUERY_1: Get all unique study_ids

select_query1 = f"SELECT " \
                f"DISTINCT ON (hashed_study_id) " \
                f"hashed_study_id, 'series_id', 'dcm_id'" \
                f"FROM {TEST_TABLE} ;"

cur.execute(select_query1)
records = cur.fetchall()

count_uniq = 0
for row in records:
    print(row)
    count_uniq += 1
print(f"Total unique study_ids: {count_uniq} ")

cur.close()
connection.close()