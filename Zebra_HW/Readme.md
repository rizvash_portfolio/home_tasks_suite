#### PRE REQVISITS:
- Python3 installed
- Docker installed  

# SETUP STEPS
-------------

## 1 STEP -> SetUp Env. 
#### Way1 - via running the bash file 'setup_jobs.sh':
1. Navigate to '/Task2_Setup_ENV' set permission to file: -> chmod +x setup_jobs.sh
2. Run setup_jobs.sh

#### Way2 - via running setup commands one by one:
1. Navigate to '/Task2_Setup_ENV' and run commands.
2. pip install -r requirements.txt
3. docker-compose up -d
4. Run 'postgres_create_test_db.py' file.
5. Run 'postgres_creat_test_table.py' file. 

## 2 STEP -> Task1 Populate_data_folders in OS
1. Navigate to '/Task1_Populate_data_folders'
2. Run 'populate_data_folders_struct.py' file

## 3 STEP -> Task3 Redis consumer producer:
1. Navigate to '/Task3_Redis'
2. Run 'redis_data_producer.py' file.
3. Run 'redis_consumer_to_db.py' file.

## 4 STEP -> Dump/ Export table:
1) PuTYY / ssh: 
 - pg_dump -d <database_name> -t <table_name> > file.sql

2) Export data from a table to CSV using COPY statement:
* COPY persons TO 'C:\tmp\persons_db.csv' DELIMITER ',' CSV HEADER;

3) Use Python lib/ Pandas / file (Task_4_crontab_Export_table/export_table_data_to_CSV.py)

#### Comands List:
> REDIS
----
* docker run -d -p 6379:6379 --name redis redis -> Start a new container Redis:
* docker run -it -p 6379:6379 redis bash -> Connecting to Redis running in Docker Container

* docker ps -> Check it's running 
* docker logs redis1  -> view the log output
* docker exec -it redis sh  -> attached to container
* redis-cli -> Run redis-cli
* ping -> "PONG" OK test connection
* KEYS * -> Retrieving All Existing Keys
* KEYS *pattern* -> Retrieving Keys by patern
* TYPE key -> Get type of key
* LRANGE `list_nme` 0 -1 -> get all values in list:

>DOCKER
------
* docker ps -aq -> List all containers (IDs)
* docker stop $(docker ps -aq) -> Stop all running containers. 
* docker rm $(docker ps -aq) -> Remove all containers 
* docker rmi $(docker images -q)  -> Remove all images

>POSTGRES
-------- 
* docker run -p 5432:5432 --name postgres -d postgres:11.3 -> Start a new container POSTGRES:
* docker exec -it postgres bash  ->  Container connection:
#### Command-line PostgreSQL
* List od DB -> \l
* Connecting to DB-> \c <database_name>
* Viewing the tables-> \d 
* The table details-> \d+ <table_name>
* List tables in current DB-> \dt
* The table description-> \d <table_name>
* Exiting the container-> \q
* DROP DATABASE database_name; 