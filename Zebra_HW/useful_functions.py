# !/usr/bin/python3

import platform
import random
import string

import psycopg2

from hw_zebra.Task2_Setup_Env.config import BASE_PATH_WINDOWS, BASE_PATH_LINUX, BASE_PATH_OSX
from hw_zebra.Task2_Setup_Env.config import FILE_EXTENSION


def random_name_stady_id(min_length=5, max_length=10):
    """
    Get a random string for file name
    Returns: concatenation of Random string of ascii characters
    """
    length = random.randint(min_length, max_length)
    return (''.join(
        random.choice(string.ascii_uppercase + string.digits)
        for _ in range(length))
    )


import uuid


def random_file_name2():
    """
    Use UUID, Universal Unique Identifier, is a python library which helps
    in generating random objects of 128 bits as ids.
    Returns: concatenation of  UUID + file extension
    """
    return (f"{str(uuid.uuid4())}" + FILE_EXTENSION)


def setup_base_path():
    """
    Set up different 'base path' for different operating system
    """

    base_os_path = ''
    if platform.system() == "Windows":
        base_os_path = BASE_PATH_WINDOWS  # "E:\\"
        return (base_os_path)
    elif platform.system() == "Linux":
        base_os_path = BASE_PATH_LINUX
        return (base_os_path)
    elif platform.system() == "Darwin":
        base_os_path = BASE_PATH_OSX
        print("OSX")
    else:
        raise Exception("Sorry, We not support your OS system!")


def hash_study_id(base_os_path: str, project_name: str) -> str:
    if base_os_path == BASE_PATH_WINDOWS:
        source_dir = base_os_path + project_name
        print(source_dir)
        pass
    else:
        pass


def get_connect_to_postgres_db(database : str, user : str, password : str, host : str, port : str):

    #         connect = psycopg2.connect(database=DB_NAME, user="postgres", password="postgres", host="localhost", port="5432")
    connect = None
    try:
        connect = psycopg2.connect(database=database, user=user,
                                   password=password, host=host, port=port)
        connect.autocommit = True
        cur = connect.cursor()
    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL", error)
        cur.close()
        connect.close()
    return (connect)


def test_import():
    print("Import works")

# if __name__ == "__main__":
#     for i in range(1):
#         name_stady_id = random_name_stady_id()
#         print(name_stady_id)
#
#     for i in range(1):
#         uuid_name = random_file_name2()
#         print(uuid_name)
#
#     setup_base_path()
#
#     base_os_path = "E:/"
#     project_name = "Test"
#     hash_study_id(base_os_path, project_name)
