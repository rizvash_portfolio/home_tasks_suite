# !/usr/bin/python3

import datetime
import os
from random import randrange

from hw_zebra.Task2_Setup_Env.config import *
from hw_zebra.useful_functions import *

base_path = setup_base_path()
# Create project dir / check: if directory exists rename old and create new one:
try:
    data_folder_path = os.path.join(base_path, PROJECT_NAME)

    if os.path.exists(data_folder_path):
        os.rename(data_folder_path, f"{data_folder_path}_old_copy_{datetime.datetime.now().strftime('%Y%m%d-%H%M%S')}")
        os.mkdir(data_folder_path)
    else:
        os.mkdir(data_folder_path)
except OSError as Error:
    print("!!! Access is denied. Probably because it is being used by another process", Error)
    raise OSError("Got an error", Error)

# list of [study folders] can be used like "primary_key" for split data for multiprocessing(different nodes).
study_folders_list = [f"stady_id{i}_" + random_name_stady_id() for i in range(1, NUMBER_OF_STUDY_DIR + 1)]

# Create folders structure
folder_path = []
for dir in study_folders_list:
    try:
        for folder_id in range(randrange(3, 6 + 1)):
            folder = os.path.join(data_folder_path, dir, f"series_id{folder_id + 1}").replace('\\', '/')
            os.makedirs(folder)
            folder_path.append(folder)

            for i in range(5):
                filename = random_file_name2()
                open(os.path.join(folder, filename), 'a').close()
    except OSError as e:
        print(e)