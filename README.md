# home_tasks_suite

Collection of home assignments.
Each folder contains a description of the task and a possible solution.

Some solutions needs refactoring and optimization.

Please read code and use it wisely.
